{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}

import Control.Lens ((.~), (^.), makeLenses)
import Control.Monad (replicateM)
import Data.Foldable (foldlM)
import qualified Data.HashMap.Strict as M
import qualified Data.Text as T
import System.Console.GetOpt
  ( ArgDescr(NoArg, ReqArg)
  , ArgOrder(Permute)
  , OptDescr(Option)
  , getOpt
  , usageInfo
  )
import System.Environment (getArgs, getProgName)
import System.Exit (exitFailure, exitSuccess)
import System.IO (stderr)
import System.Random.Stateful (uniformRM)
import Text.Printf (hPrintf, printf)

import Random (withURandom)
import WordList (passwords)

data Options = Options
  { _optNumWords :: Int
  }

makeLenses ''Options

generateWord :: M.HashMap Int T.Text -> Int -> T.Text
generateWord m index = maybe "UNDEFINED" id $ M.lookup index m

options :: [OptDescr (Options -> IO Options)]
options =
  [ Option
      ['n']
      ["num_words"]
      (ReqArg (\n opts -> return $ (optNumWords .~ read n) opts) "N")
      "number of words to generate"
  , Option
      ['h']
      ["help"]
      (NoArg . const $ do
         p <- getProgName
         printf
           "%s"
           (usageInfo (printf "Usage: %s [--num_words=N] [--help]" p) options)
         exitSuccess)
      "print usage info"
  ]

defaultOptions :: Options
defaultOptions = Options {_optNumWords = 6}

main :: IO ()
main = do
  args <- getArgs
  let (o, nonOpts, errs) = getOpt Permute options args
  if not (null errs)
    then mapM (hPrintf stderr "%s") errs >> exitFailure
    else return ()
  if not (null nonOpts)
    then hPrintf
           stderr
           "Unexpected positional arguments: %s.\n"
           (unwords nonOpts) >>
         exitFailure
    else return ()
  opts <- foldlM (flip id) defaultOptions o
  indexList <-
    withURandom (\g -> replicateM (opts ^. optNumWords) $ uniformRM (0, 7775) g)
  printf "%s\n" $ T.unwords (map (generateWord passwords) indexList)
