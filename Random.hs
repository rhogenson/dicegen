{-# LANGUAGE MultiParamTypeClasses #-}

module Random
  ( withURandom
  ) where

import qualified Data.ByteString as B
import Data.Word (Word64)
import System.IO (Handle, IOMode(ReadMode), withBinaryFile)
import System.Random.Stateful (StatefulGen, uniformWord64)

newtype URandom =
  URandom Handle

readInt :: B.ByteString -> Word64
readInt = B.foldl' (\acc x -> acc * 2 ^ (8 :: Int) + fromIntegral x) 0

instance StatefulGen URandom IO where
  uniformWord64 (URandom h) = readInt <$> B.hGet h 8

withURandom :: (URandom -> IO a) -> IO a
withURandom f = withBinaryFile "/dev/urandom" ReadMode (f . URandom)
