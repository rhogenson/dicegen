{-# LANGUAGE CPP, QuasiQuotes #-}

module RawWordList (wordlist) where

import Text.RawString.QQ (r)

wordlist :: String
wordlist = [r|
#include "diceware.wordlist.asc"
|]
