module WordList
  ( passwords
  ) where

import qualified Data.HashMap.Strict as M
import Data.List (foldl')
import qualified Data.Text as T

import RawWordList (wordlist)

parseDigit :: Char -> Maybe Int
parseDigit '1' = Just 1
parseDigit '2' = Just 2
parseDigit '3' = Just 3
parseDigit '4' = Just 4
parseDigit '5' = Just 5
parseDigit '6' = Just 6
parseDigit _ = Nothing

diceToInt :: T.Text -> Maybe Int
diceToInt t =
  T.foldl'
    (\acc x ->
       case acc of
         Just a
           | Just dg <- parseDigit x -> Just $ a * 6 + dg - 1
         _ -> Nothing)
    (Just 0)
    t

readPasswords :: T.Text -> M.HashMap Int T.Text
readPasswords s =
  foldl'
    (\acc x ->
       case T.words x of
         [key, value]
           | Just ki <- diceToInt key -> M.insert ki value acc
         _ -> acc)
    M.empty $
  T.lines s

passwords :: M.HashMap Int T.Text
passwords = readPasswords (T.pack wordlist)
