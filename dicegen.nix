{ mkDerivation, base, bytestring, directory, filepath, hashmap
, stdenv, text
}:
mkDerivation {
  pname = "dicegen";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base bytestring directory filepath hashmap text
  ];
  homepage = "rayhogenson.github.io";
  description = "Generate diceware passwords";
  license = stdenv.lib.licenses.gpl3;
}
